﻿using InsuranceOfficeAPI.Models;
using Microsoft.EntityFrameworkCore;

namespace InsuranceOfficeAPI.Context
{
    public class AppDbContext: DbContext
    {
        public AppDbContext()
        {
        }

        public AppDbContext(DbContextOptions<AppDbContext> options):base(options) 
        {
        
        }

        public DbSet<UserDto> Users { get; set; }
        public DbSet<CustomerDto> Customers { get; set; }
        public DbSet<InsuranceDto> Insurances { get; set; }
        public DbSet<PolicyDto> Policies { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder) 
        {
            modelBuilder.Entity<UserDto>().ToTable("users");
            modelBuilder.Entity<CustomerDto>().ToTable("customers");
            modelBuilder.Entity<InsuranceDto>().ToTable("insurances");
            modelBuilder.Entity<PolicyDto>().ToTable("policies");
        }

    }
}
