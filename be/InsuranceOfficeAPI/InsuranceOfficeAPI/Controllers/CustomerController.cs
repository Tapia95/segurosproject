﻿using InsuranceOfficeAPI.Context;
using InsuranceOfficeAPI.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Globalization;
using System.Text.RegularExpressions;

namespace InsuranceOfficeAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : Controller
    {

        private readonly AppDbContext _appContext;

        public CustomerController(AppDbContext appDbContext)
        {
            _appContext = appDbContext;
        }

        [Authorize]
        [HttpPost("createOrUpdate")]
        public async Task<IActionResult> CreateOrEdit([FromBody] CustomerDto customerObj)
        {
            if (customerObj == null)
            {
                return Ok(new
                {
                    Code = "001",
                    Message = "Formato de parámetros de entrada no valido!"
                });
            }

            if (customerObj.Id == 0)
            {
                var existeCustomer = await _appContext.Customers.Where(x => x.Document == customerObj.Document).FirstOrDefaultAsync();
                if (existeCustomer != null)
                {
                    return Ok(new
                    {
                        Code = "001",
                        Message = "Ya existe un cliente con la cédula ingresada!"
                    });
                } 
                else
                {
                    await _appContext.Customers.AddAsync(customerObj);
                    await _appContext.SaveChangesAsync();
                }
            }
            else
            {
                var existeCustomer = await _appContext.Customers.Where(x => x.Document == customerObj.Document && x.Id != customerObj.Id).FirstOrDefaultAsync();
                if (existeCustomer != null)
                {
                    return Ok(new
                    {
                        Code = "001",
                        Message = "Ya existe un cliente con la cédula ingresada!"
                    });
                }
                else
                {
                    var result = await _appContext.Customers.Where(x => x.Id == customerObj.Id).FirstOrDefaultAsync();
                    if (result != null)
                    {
                        result.Name = customerObj.Name;
                        result.Document = customerObj.Document;
                        result.Gender = customerObj.Gender;
                        result.Address = customerObj.Address;
                        result.Email = customerObj.Email;
                        result.Phone = customerObj.Phone;
                        result.Birthdate = customerObj.Birthdate;
                        await _appContext.SaveChangesAsync();
                    }
                    else
                    {
                        return Ok(new
                        {
                            Code = "001",
                            Message = "No existe el cliente ha editar!"
                        });
                    }
                }
            }

            return Ok(new
            {
                Code = "000",
                Message = "Cliente guardado correctamente!"
            });
        }

        [Authorize]
        [HttpPost("importCustomers")]
        public async Task<IActionResult> ImportCustomers([FromBody] List<CustomerImportDto> customerObj)
        {
            if (customerObj == null)
            {
                return Ok(new
                {
                    Code = "001",
                    Message = "Formato de parámetros de entrada no valido!"
                });
            }

            if (customerObj.Count <= 0)
            {
                return Ok(new
                {
                    Code = "001",
                    Message = "Lista de clientes ha importar se encuentra vacia!"
                });
            }

            string code = "000";
            string message = "No se completo el proceso por favor verificar la información!";
            int addCustomers = 0;
            int editCustomers = 0;
            int errorCustomers = 0;

            try
            {
                foreach (var item in customerObj)
                {
                    var isValidModel = validateImportModel(item);
                    if (isValidModel)
                    {
                        var existeCustomer = await _appContext.Customers.Where(x => x.Document == item.Cedula).FirstOrDefaultAsync();
                        if (existeCustomer != null)
                        {
                            existeCustomer.Name = item.Nombre;
                            existeCustomer.Document = item.Cedula;
                            existeCustomer.Gender = item.Genero;
                            existeCustomer.Address = item.Direccion;
                            existeCustomer.Email = item.Correo;
                            existeCustomer.Phone = item.Telefono;
                            existeCustomer.Birthdate = DateTime.Parse(item.Fnacimiento.ToString());
                            await _appContext.SaveChangesAsync();
                            editCustomers++;
                        }
                        else
                        {
                            CustomerDto addCustomer = new CustomerDto();
                            addCustomer.Name = item.Nombre;
                            addCustomer.Document = item.Cedula;
                            addCustomer.Gender = item.Genero;
                            addCustomer.Address = item.Direccion;
                            addCustomer.Email = item.Correo;
                            addCustomer.Phone = item.Telefono;
                            addCustomer.Birthdate = DateTime.Parse(item.Fnacimiento.ToString());
                            await _appContext.Customers.AddAsync(addCustomer);
                            await _appContext.SaveChangesAsync();
                            addCustomers++;
                        }
                    }
                    else
                    {
                        errorCustomers++;
                    }
                }

                if (addCustomers <= 0 && editCustomers <= 0)
                {
                    code = "001";
                    message = "No se logro agregar los clientes: \nCliente agregado: " + addCustomers + " \nClientes editados: " + editCustomers + " \nClientes fallidos: " + errorCustomers;
                } 
                else
                {
                    message = "Proceso completado: \nCliente agregado: " + addCustomers + " \nClientes editados: " + editCustomers + " \nClientes fallidos: " + errorCustomers;                    
                }
            }
            catch(Exception ex)
            {
                code = "001";
                message = message + " \n Excepcion " + ex.Message;
            }

            return Ok(new
            {
                Code = code,
                Message = message,
                AddCustomers = addCustomers,
                EditCustomers = editCustomers,
                ErrorCustomers = errorCustomers,
            });
        }       

        [Authorize]
        [HttpDelete("delete")]
        public async Task<IActionResult> delete(int id)
        {
            string message = "Cliente eliminado con exito!";
            string code = "000";

            if (id <= 0)
            {
                return Ok(new
                {
                    Code = "001",
                    Message = "Formato de parámetros de entrada no valido!"
                });
            }

            var customer = await _appContext.Customers.Where(b => b.Id == id).FirstOrDefaultAsync();
            if (customer != null)
            {
                var existPolicy = await _appContext.Policies.Where(b => b.CustomerId == customer.Id).FirstOrDefaultAsync();
                if (existPolicy != null)
                {
                    code = "001";
                    message = "Ya existe una póliza asociada a este cliente";
                }
                else
                {
                    _appContext.Customers.Remove(customer);
                    await _appContext.SaveChangesAsync();
                }                
            }
            else
            {
                code = "001";
                message = "No existe el cliente a eliminar!";
            }            

            return Ok(new
            {
                Code = code,
                Message = message
            });
        }

        [Authorize]
        [HttpGet]
        public async Task<ActionResult<CustomerDto>> GetAll()
        {
            return Ok(await _appContext.Customers.ToListAsync());
        }

        private bool validateImportModel(CustomerImportDto customerObj)
        {
            string patternNumber = "^[0-9]+$";
            string patternCharacters = "^[a-zA-ZÀ-ÿ\\s]{4,64}$";
            string patternAddress = "^[a-zA-Z0-9À-ÿ\\s]{0,256}$";
            string patternEmail = "^\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,3})+$";
            if (string.IsNullOrEmpty(customerObj.Cedula) || customerObj.Cedula.Trim().Length != 10 || !Regex.IsMatch(customerObj.Cedula, patternNumber))
            {
                return false;
            }

            if (string.IsNullOrEmpty(customerObj.Genero) || customerObj.Genero.Trim().Length != 1 || (!customerObj.Genero.Equals("M") && !customerObj.Genero.Equals("F")))
            {
                return false;
            }

            if (string.IsNullOrEmpty(customerObj.Nombre) || customerObj.Nombre.Trim().Length < 4 || customerObj.Nombre.Trim().Length > 64 || !Regex.IsMatch(customerObj.Nombre, patternCharacters))
            {
                return false;
            }

            if (string.IsNullOrEmpty(customerObj.Direccion) || customerObj.Direccion.Trim().Length > 256 || !Regex.IsMatch(customerObj.Direccion, patternAddress))
            {
                return false;
            }

            if (string.IsNullOrEmpty(customerObj.Correo) || customerObj.Direccion.Trim().Length > 64 || !Regex.IsMatch(customerObj.Correo, patternEmail))
            {
                return false;
            }

            if (string.IsNullOrEmpty(customerObj.Telefono) || customerObj.Telefono.Trim().Length != 10 || !Regex.IsMatch(customerObj.Telefono, patternNumber))
            {
                return false;
            }

            if (string.IsNullOrEmpty(customerObj.Fnacimiento))
            {
                return false;
            }

            try
            {
                DateTime dt = DateTime.ParseExact(customerObj.Fnacimiento, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                if (dt < DateTime.Now.AddYears(-100) || dt > DateTime.Now.AddYears(18))
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }

            return true;
        }

    }
}
