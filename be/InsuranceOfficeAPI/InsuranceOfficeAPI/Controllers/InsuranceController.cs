﻿using InsuranceOfficeAPI.Context;
using InsuranceOfficeAPI.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;

namespace InsuranceOfficeAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InsuranceController : Controller
    {

        private readonly AppDbContext _appContext;

        public InsuranceController(AppDbContext appDbContext)
        {
            _appContext = appDbContext;
        }

        [Authorize]
        [HttpPost("createOrUpdate")]
        public async Task<IActionResult> CreateOrEdit([FromBody] InsuranceDto insuranceObj)
        {

            if (insuranceObj == null)
            {
                return Ok(new
                {
                    Code = "001",
                    Message = "Formato de parámetros de entrada no valido!"
                });
            }

            if (insuranceObj.Id == 0)
            {
                var existeInsurance = await _appContext.Insurances.Where(x => x.Code == insuranceObj.Code || x.Name == insuranceObj.Name).FirstOrDefaultAsync();
                if (existeInsurance != null)
                {
                    return Ok(new
                    {
                        Code = "001",
                        Message = "Ya existe un séguro con el código/nombre ingresado!"
                    });
                }
                else
                {
                    await _appContext.Insurances.AddAsync(insuranceObj);
                    await _appContext.SaveChangesAsync();
                }
            } 
            else
            {
                var existeInsurance = await _appContext.Insurances.Where(x => (x.Code == insuranceObj.Code || x.Name == insuranceObj.Name) && x.Id != insuranceObj.Id).FirstOrDefaultAsync();
                if (existeInsurance != null)
                {
                    return Ok(new
                    {
                        Code = "001",
                        Message = "Ya existe un séguro con el código/nombre ingresado!"
                    });
                }
                else
                {
                    var result = await _appContext.Insurances.Where(x => x.Id == insuranceObj.Id).FirstOrDefaultAsync();
                    if (result != null)
                    {
                        result.Name = insuranceObj.Name;
                        result.Code = insuranceObj.Code;
                        result.Type = insuranceObj.Type;
                        result.InsuredValue = insuranceObj.InsuredValue;
                        result.Bonus = insuranceObj.Bonus;
                        result.Observations = insuranceObj.Observations;
                        await _appContext.SaveChangesAsync();
                    } 
                    else
                    {
                        return Ok(new
                        {
                            Code = "001",
                            Message = "No existe el seguro ha editar!"
                        });
                    }
                }
            }

            return Ok(new
            {
                Code = "000",
                Message = "Seguro guardado correctamente!"
            });
        }

        [Authorize]
        [HttpDelete("delete")]
        public async Task<IActionResult> delete(int id)
        {
            string message = "Seguro eliminado con exito!";
            string code = "000";

            if (id <= 0)
            {
                return Ok(new
                {
                    Code = "001",
                    Message = "Formato de parámetros de entrada no valido!"
                });
            }

            var insurance = await _appContext.Insurances.Where(b => b.Id == id).FirstOrDefaultAsync();
            if (insurance != null)
            {
                var existPolicy = await _appContext.Policies.Where(b => b.InsuranceId == insurance.Id).FirstOrDefaultAsync();
                if (existPolicy != null)
                {
                    code = "001";
                    message = "Ya existe una póliza asociada a este seguro";
                } 
                else
                {
                    _appContext.Insurances.Remove(insurance);
                    await _appContext.SaveChangesAsync();
                }                
            } 
            else
            {
                message = "No existe el seguro a eliminar!";
            }            

            return Ok(new
            {
                Code = code,
                Message = message
            });
        }

        [Authorize]
        [HttpGet]
        public async Task<ActionResult<InsuranceDto>> GetAll()
        {
            return Ok(await _appContext.Insurances.ToListAsync());
        }

    }
}
