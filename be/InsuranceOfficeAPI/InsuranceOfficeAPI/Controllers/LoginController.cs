﻿using InsuranceOfficeAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using System.Data;

namespace InsuranceOfficeAPI.Controllers
{
    public class LoginController : Controller
    {

        const string SessionUser = "_User";

        public IConfiguration Configuration { get; }

        public LoginController(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        //public ActionResult Login()
        //{

        //    //ViewBag.ReturnUrl = returnUrl;
        //    return View(new UserDto());
        //}


        //[HttpPost]
        //public ActionResult Login(UserDto model) 
        //{
            //string connectionString = Configuration["ConnectionStrings:DefaultConnection"];

            //using (SqlConnection connection = new SqlConnection(connectionString))
            //{
            //    var list_users = new List<UserDto>();

            //    if (string.IsNullOrEmpty(model.username) || string.IsNullOrEmpty(model.password))
            //    {
            //        ModelState.AddModelError("", "Ingresar los datos solictiados");
            //    } 
            //    else
            //    {
            //        connection.Open();
            //        SqlCommand com = new SqlCommand("GET_SEG_USER", connection);
            //        com.CommandType = CommandType.StoredProcedure;

            //        com.Parameters.AddWithValue("@user", model.username);
            //        com.Parameters.AddWithValue("@password", model.password);

            //        SqlDataReader dr = com.ExecuteReader();
            //        while (dr.Read())
            //        {
            //            UserDto user = new UserDto();
            //            user.username = Convert.ToString(dr["Username"]);
            //            user.password = Convert.ToString(dr["Password"]);
            //            list_users.Add(user);
            //        }

            //        if (list_users.Any(p => p.username == model.username && p.password == model.password))
            //        {
            //            HttpContext.Session.SetString(SessionUser, model.username);
            //            return RedirectToAction("Usuario", "Home");
            //        }
            //        else
            //        {
            //            ModelState.AddModelError("", "Usuario o clave incorrecta. Por favor verificar la información!");
            //        }                    
            //    }
            //    return View(model);
            //}
        //}


        [HttpPost]
        public ActionResult LogOut()
        {
            HttpContext.Session.Clear();
            return RedirectToAction("Login", "Login");
        }


        public IActionResult Index()
        {
            return View();
        }
    }
}
