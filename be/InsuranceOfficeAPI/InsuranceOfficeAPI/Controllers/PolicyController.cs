﻿using InsuranceOfficeAPI.Context;
using InsuranceOfficeAPI.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Text.RegularExpressions;

namespace InsuranceOfficeAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PolicyController : Controller
    {

        private readonly AppDbContext _appContext;

        public PolicyController(AppDbContext appDbContext)
        {
            _appContext = appDbContext;
        }

        [Authorize]
        [HttpGet]
        public async Task<ActionResult<PolicyDto>> GetAll()
        {
            return Ok(await _appContext.Policies.ToListAsync());
        }

        [Authorize]
        [HttpPost("CreateOrUpdate")]
        public async Task<ActionResult<PolicyDto>> CreateOrUpdate([FromBody] List<PolicyDto> policies)
        {
            if (policies == null) return BadRequest();

            if (policies.Count < 1) return BadRequest();

            var nullObjForeign = policies.Where(x => x.CustomerId <= 0 || x.InsuranceId <= 0).FirstOrDefault();
            if (nullObjForeign != null) return BadRequest();

            foreach (var item in policies)
            {
                var customer = await _appContext.Customers.FindAsync(item.CustomerId);
                if (customer == null) return BadRequest();

                var insurance = await _appContext.Insurances.FindAsync(item.InsuranceId);
                if (insurance == null) return BadRequest();

                string sequentialNumber = await getSequentialPolicy();

                item.PolicyNumber = sequentialNumber;

                await _appContext.Policies.AddAsync(item);
                await _appContext.SaveChangesAsync();
            }

            return Ok(new
            {
                Message = "Cliente guardado correctamente!"
            });
        }

        private async Task<string> getSequentialPolicy()
        {
            string sequential = "00000000";
            var lastPolicy = await _appContext.Policies.OrderByDescending(p => p.Id).FirstOrDefaultAsync();
            if (lastPolicy == null) return sequential;
            else
            {
                string lastSequential = lastPolicy.PolicyNumber;
                int sequentialInt = int.Parse(lastSequential);
                sequentialInt = sequentialInt + 1;

                string sequentialStr = sequentialInt.ToString();
                if (sequentialStr.Length < 8)
                {
                    int zeroToAdd = 9 - sequentialStr.Length;
                    sequentialStr = sequentialStr.PadLeft(zeroToAdd, '0');                
                }

                return sequentialStr;
            }
        }

        [Authorize]
        [HttpPost("FindAll")]
        public async Task<ActionResult<PolicyDto>> FindAll()
        {
            var policies = (from policy in _appContext.Policies
                            join customerData in _appContext.Customers on policy.CustomerId equals customerData.Id
                            join insuranceData in _appContext.Insurances on policy.InsuranceId equals insuranceData.Id                            
                            select new PoliciesDto
                            {
                                Id = policy.Id,
                                PolicyNumber = policy.PolicyNumber,
                                DocumentCustomer = customerData.Document,
                                NameCustomer = customerData.Name,
                                InsuranceCode = insuranceData.Code,
                                InsuranceName = insuranceData.Name,
                                InsuranceType = insuranceData.Type,
                                InsuredValue = insuranceData.InsuredValue,
                                Observations = insuranceData.Observations,
                            });

            return Ok(await policies.ToListAsync());
        }

        [Authorize]       
        [HttpPost("FindByDocument")]
        public async Task<ActionResult<PolicyDto>> FindByDocument([FromBody] FilterDocumentDto filter)
        {
            if (filter == null)
            {
                return Ok(new
                {
                    Code = "001",
                    Message = "Formato de parámetros de entrada no valido!"
                });
            }

            string pattern = "^[0-9]+$";
            if (string.IsNullOrEmpty(filter.Document) || filter.Document.Length != 10 || !Regex.IsMatch(filter.Document, pattern))
            {
                return Ok(new
                {
                    Code = "001",
                    Message = "Formato de parámetros de entrada no valido!"
                });
            }

            var customer = (from usr in _appContext.Customers
                           where usr.Document == filter.Document
                           select usr).FirstOrDefault();

            if (customer == null)
            {
                return Ok(new
                {
                    Code = "001",
                    Message = "Cliente no existe!"
                });
            }

            var policies = (from policy in _appContext.Policies
                            join customerData in _appContext.Customers on policy.CustomerId equals customerData.Id
                            join insuranceData in _appContext.Insurances on policy.InsuranceId equals insuranceData.Id
                            where policy.CustomerId == customer.Id
                            select new PoliciesDto
                            {
                                Id = policy.Id,
                                PolicyNumber = policy.PolicyNumber,
                                DocumentCustomer = customerData.Document,
                                NameCustomer = customerData.Name,
                                InsuranceCode = insuranceData.Code,
                                InsuranceName = insuranceData.Name,
                                InsuranceType = insuranceData.Type,
                                InsuredValue = insuranceData.InsuredValue,
                                Observations = insuranceData.Observations,
                            });

            var policiesList = policies != null ? policies.ToList() : null;

            if (policiesList == null || policiesList.Count <= 0)
            {
                return Ok(new
                {
                    Code = "001",
                    Message = "No existen pólizas para la cédula consultada!!"
                });
            }

            return Ok(new
            {
                Code = "000",
                Message = "Pólizas consultadas con exito!!",
                Data = await policies.ToListAsync()
            });
        }

        [Authorize]
        [HttpPost("FindByCodeInsurance")]
        public async Task<ActionResult<PolicyDto>> FindByCodeInsurance([FromBody] FilterCodeInsuranceDto filter)
        {
            if (filter == null)
            {
                return Ok(new
                {
                    Code = "001",
                    Message = "Formato de parámetros de entrada no valido!"
                });
            }

            string pattern = "^[a-zA-Z0-9À-ÿ\\s]{2,8}$";
            if (string.IsNullOrEmpty(filter.Code) || filter.Code.Length < 2 || filter.Code.Length > 8 || !Regex.IsMatch(filter.Code, pattern))
            {
                return Ok(new
                {
                    Code = "001",
                    Message = "Formato de parámetros de entrada no valido!"
                });
            }

            var insurance = (from ins in _appContext.Insurances
                            where ins.Code == filter.Code
                            select ins).FirstOrDefault();

            if (insurance == null)
            {
                return Ok(new
                {
                    Code = "001",
                    Message = "No existe seguros con el codigo ingresado!"
                });
            }

            var policies = (from policy in _appContext.Policies
                            join customerData in _appContext.Customers on policy.CustomerId equals customerData.Id
                            join insuranceData in _appContext.Insurances on policy.InsuranceId equals insuranceData.Id
                            where policy.InsuranceId == insurance.Id
                            select new PoliciesDto
                            {
                                Id = policy.Id,
                                PolicyNumber = policy.PolicyNumber,
                                DocumentCustomer = customerData.Document,
                                NameCustomer = customerData.Name,
                                InsuranceCode = insuranceData.Code,
                                InsuranceName = insuranceData.Name,
                                InsuranceType = insuranceData.Type,
                                InsuredValue = insuranceData.InsuredValue,
                                Observations = insuranceData.Observations,
                            });

            var policiesList = policies != null ? policies.ToList() : null;

            if (policiesList == null || policiesList.Count <= 0)
            {
                return Ok(new
                {
                    Code = "001",
                    Message = "No existen pólizas para la cédula consultada!!"
                });
            }

            return Ok(new
            {
                Code = "000",
                Message = "Pólizas consultadas con exito!!",
                Data = await policies.ToListAsync()
            });
        }

        [Authorize]
        [HttpDelete("delete")]
        public async Task<IActionResult> delete(int id)
        {
            string message = "Póliza eliminada con exito!";

            if (id <= 0) return BadRequest();

            var policy = await _appContext.Policies.Where(b => b.Id == id).FirstOrDefaultAsync();
            if (policy != null)
            {
                _appContext.Policies.Remove(policy);
                await _appContext.SaveChangesAsync();
            }
            else
            {
                message = "No existe la póliza a eliminar!";
            }

            return Ok(new
            {
                Message = message
            });
        }

    }
}
