﻿using Azure.Messaging;
using InsuranceOfficeAPI.Context;
using InsuranceOfficeAPI.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace InsuranceOfficeAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly AppDbContext _appContext;
        public UserController(AppDbContext appDbContext)
        {
            _appContext = appDbContext;
        }

        [HttpPost("authenticate")]
        public async Task<IActionResult> Authenticate([FromBody] UserDto userObj)
        {
            if (userObj == null) return BadRequest();

            var user = await _appContext.Users.FirstOrDefaultAsync(x => x.Username == userObj.Username && x.Password == userObj.Password);
            if (user == null) return NotFound(new { Message = "Usuario no valido! Por favor verificar usuario o contraseña" });

            user.Token = CreateJWT(user);

            return Ok(new 
            { 
                Token = user.Token,
                Message = "Inicio de sesion realizado correctamente!" 
            });
        }

        [HttpPost("register")]
        public async Task<IActionResult> RegisterUser([FromBody] UserDto userObj)
        {
            if (userObj == null) return BadRequest();

            await _appContext.Users.AddAsync(userObj);
            await _appContext.SaveChangesAsync();

            return Ok(new
            {
                Message = "Usuario registrado!"
            });
        }

        [Authorize]
        [HttpGet]
        public async Task<ActionResult<UserDto>> GetAllUsers()
        {
            return Ok(await _appContext.Users.ToListAsync());
        }

        private string CreateJWT(UserDto user)
        {
            var jwtToken = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes("LLAVEJSONWEBTOKEN");
            var identity = new ClaimsIdentity(new Claim[]
            {
            new Claim(ClaimTypes.Role, user.Role),
            new Claim(ClaimTypes.Name, user.Name)
            });

            var credentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = identity,
                Expires = DateTime.Now.AddDays(1),
                SigningCredentials = credentials
            };

            var token = jwtToken.CreateToken(tokenDescriptor);

            return jwtToken.WriteToken(token);
        }


    }

}
