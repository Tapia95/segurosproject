﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace InsuranceOfficeAPI.Migrations
{
    /// <inheritdoc />
    public partial class AddFieldNames : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "users",
                type: "nvarchar(max)",
                nullable: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Name",
                table: "users");
        }
    }
}
