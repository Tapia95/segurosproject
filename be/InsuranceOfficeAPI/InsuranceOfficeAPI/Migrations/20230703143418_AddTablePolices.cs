﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace InsuranceOfficeAPI.Migrations
{
    /// <inheritdoc />
    public partial class AddTablePolices : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "polices",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CustomerId = table.Column<int>(type: "int", nullable: false),
                    InsuranceId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_polices", x => x.Id);
                    table.ForeignKey(
                        name: "FK_polices_customers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "customers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_polices_insurances_InsuranceId",
                        column: x => x.InsuranceId,
                        principalTable: "insurances",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_polices_CustomerId",
                table: "polices",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_polices_InsuranceId",
                table: "polices",
                column: "InsuranceId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "polices");
        }
    }
}
