﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace InsuranceOfficeAPI.Migrations
{
    /// <inheritdoc />
    public partial class AddFieldPolicyNumber : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "PolicyNumber",
                table: "polices",
                type: "nvarchar(max)",
                nullable: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PolicyNumber",
                table: "polices");
        }
    }
}
