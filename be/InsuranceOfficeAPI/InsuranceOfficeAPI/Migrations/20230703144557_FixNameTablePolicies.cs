﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace InsuranceOfficeAPI.Migrations
{
    /// <inheritdoc />
    public partial class FixNameTablePolicies : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_polices_customers_CustomerId",
                table: "polices");

            migrationBuilder.DropForeignKey(
                name: "FK_polices_insurances_InsuranceId",
                table: "polices");

            migrationBuilder.DropPrimaryKey(
                name: "PK_polices",
                table: "polices");

            migrationBuilder.RenameTable(
                name: "polices",
                newName: "policies");

            migrationBuilder.RenameIndex(
                name: "IX_polices_InsuranceId",
                table: "policies",
                newName: "IX_policies_InsuranceId");

            migrationBuilder.RenameIndex(
                name: "IX_polices_CustomerId",
                table: "policies",
                newName: "IX_policies_CustomerId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_policies",
                table: "policies",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_policies_customers_CustomerId",
                table: "policies",
                column: "CustomerId",
                principalTable: "customers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_policies_insurances_InsuranceId",
                table: "policies",
                column: "InsuranceId",
                principalTable: "insurances",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_policies_customers_CustomerId",
                table: "policies");

            migrationBuilder.DropForeignKey(
                name: "FK_policies_insurances_InsuranceId",
                table: "policies");

            migrationBuilder.DropPrimaryKey(
                name: "PK_policies",
                table: "policies");

            migrationBuilder.RenameTable(
                name: "policies",
                newName: "polices");

            migrationBuilder.RenameIndex(
                name: "IX_policies_InsuranceId",
                table: "polices",
                newName: "IX_polices_InsuranceId");

            migrationBuilder.RenameIndex(
                name: "IX_policies_CustomerId",
                table: "polices",
                newName: "IX_polices_CustomerId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_polices",
                table: "polices",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_polices_customers_CustomerId",
                table: "polices",
                column: "CustomerId",
                principalTable: "customers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_polices_insurances_InsuranceId",
                table: "polices",
                column: "InsuranceId",
                principalTable: "insurances",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
