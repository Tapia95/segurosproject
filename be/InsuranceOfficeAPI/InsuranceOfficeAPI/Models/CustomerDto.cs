﻿using System.ComponentModel.DataAnnotations;

namespace InsuranceOfficeAPI.Models
{
    public class CustomerDto
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Document { get; set; }
        public string Gender { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public DateTime Birthdate { get; set; }
    }
}
