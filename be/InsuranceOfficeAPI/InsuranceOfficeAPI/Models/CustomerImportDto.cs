﻿namespace InsuranceOfficeAPI.Models
{
    public class CustomerImportDto
    {
        public string Cedula { get; set;}
        public string Nombre { get; set;}
        public string Genero { get; set;}
        public string Direccion { get; set;}
        public string Correo { get; set;}
        public string Telefono { get; set;}
        public string Fnacimiento { get; set;}
    }
}
