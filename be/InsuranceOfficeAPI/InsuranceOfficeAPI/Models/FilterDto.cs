﻿namespace InsuranceOfficeAPI.Models
{
    public class FilterCodeInsuranceDto
    {
        public string Code { get; set; }
    }

    public class FilterDocumentDto
    {
        public string Document { get; set; }
    }            
}
