﻿using System.ComponentModel.DataAnnotations;

namespace InsuranceOfficeAPI.Models
{
    public class InsuranceDto
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Type { get; set; }
        public double InsuredValue { get; set; }
        public double Bonus { get; set; }
        public string Observations { get; set; }
    }
}
