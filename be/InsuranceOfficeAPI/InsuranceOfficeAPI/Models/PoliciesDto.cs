﻿namespace InsuranceOfficeAPI.Models
{
    public class PoliciesDto
    {
        public int Id { get; set; }
        public string PolicyNumber { get; set; }
        public string DocumentCustomer { get; set; }
        public string NameCustomer { get; set; }
        public string InsuranceCode { get; set; }
        public string InsuranceName { get; set; }
        public string InsuranceType { get; set; }
        public double InsuredValue { get; set; }
        public string Observations { get; set; }
    }
}
