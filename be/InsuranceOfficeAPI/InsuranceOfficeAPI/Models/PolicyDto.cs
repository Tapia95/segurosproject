﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace InsuranceOfficeAPI.Models
{
    public class PolicyDto
    {
        [Key]
        public int Id { get; set; }

        public string PolicyNumber { get; set; }
        
        public int CustomerId { get; set; }
        [ForeignKey("CustomerId")]
        public CustomerDto Customer { get; set; }

        public int InsuranceId { get; set; }
        [ForeignKey("InsuranceId")]
        public InsuranceDto Insurance { get; set; }
    }
}
