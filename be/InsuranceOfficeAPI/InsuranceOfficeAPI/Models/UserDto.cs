﻿using System.ComponentModel.DataAnnotations;

namespace InsuranceOfficeAPI.Models
{
    public class UserDto
    {
        [Key]
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Token { get; set; }
        public string Role { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Name { get; set; }
    }
}
