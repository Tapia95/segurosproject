import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import {
  HTTP_INTERCEPTORS,
  HttpClientModule,
} from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TokenInterceptor } from './interceptors/token.interceptor';
import { CreateCustomerComponent } from './components/customer/create-customer.component';
import { MaterialModule } from './material/material.module';
import { NavBarComponent } from './components/layout/nav-bar/nav-bar.component';
import { SharedModule } from './module/shared.module';
import { LoginComponent } from './components/login/login.component';
import { CustomerComponent } from './components/customer/customer.component';
import { InsuranceComponent } from './components/insurance/insurance.component';
import { CreateInsuranceComponent } from './components/insurance/create-insurance.component';
import { PoliciesComponent } from './components/policies/policies.component';
import { CreatePoliciesComponent } from './components/policies/create-policies.component';
import { PoliciesViewComponent } from './components/policies/policies-view.component';
import { SpinnerComponent } from './components/layout/spinner/spinner.component';
import { LoadingInterceptor } from './interceptors/loading.interceptor';
import { BadgeComponent } from './components/layout/badge/badge.component';

@NgModule({
  declarations: [
    NavBarComponent,
    AppComponent,
    LoginComponent,
    CustomerComponent,
    CreateCustomerComponent,
    InsuranceComponent,
    CreateInsuranceComponent,
    PoliciesComponent,
    CreatePoliciesComponent,
    PoliciesViewComponent,
    SpinnerComponent,
    BadgeComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    HttpClientModule,
    NgbModule,
    MaterialModule,
    SharedModule,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LoadingInterceptor,
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
