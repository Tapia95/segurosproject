import { Component, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ApiService } from 'src/app/services/api.service';
import { CustomerI } from 'src/app/services/interfaces/customer.interface';
import { SharedService } from 'src/app/services/shared.service';
import { Utils } from 'src/app/shared/utils';
import Swal from 'sweetalert2';
import { DateAdapter, MAT_DATE_LOCALE } from '@angular/material/core';

@Component({
  selector: 'app-create-customer',
  templateUrl: './create-customer.component.html',
  styleUrls: ['./create-customer.component.css'],
  providers: [
    {provide: MAT_DATE_LOCALE, useValue: 'es-AR'},
  ],
})
export class CreateCustomerComponent {

  defaultMessage: any = Utils.defaultMessages;
  form!: FormGroup;
  title = 'CREAR';

  minDate: Date;
  maxDate: Date;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: CustomerI,
    @Inject(MAT_DATE_LOCALE) private _locale: string,
    public dialogRef: MatDialogRef<CreateCustomerComponent>,    
    private _sharedService: SharedService,
    private _adapter: DateAdapter<any>,
    private formBuilder: FormBuilder,
    private api: ApiService,
  ) {
    this._locale = 'es-ES';
    this._adapter.setLocale(this._locale);

    const currentYear = new Date().getFullYear();
    const currentMonth = new Date().getMonth();
    const currentDay = new Date().getDay();
    this.minDate = new Date(currentYear - 100, 0, 1);
    this.maxDate = new Date(currentYear, currentMonth, currentDay);
  }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      id: [0],
      document: ['',
        [
          Validators.required,
          Validators.maxLength(10),
          Validators.minLength(10),
          Validators.pattern(Utils.regex.document)
        ]],
      gender: ['',
        [
          Validators.required,
          Validators.maxLength(1),
          Validators.minLength(1),          
        ]],
      name: ['',
        [
          Validators.required,
          Validators.maxLength(64),
          Validators.minLength(4),
          Validators.pattern(Utils.regex.name)
        ]],
      address: ['',
        [
          Validators.required,
          Validators.maxLength(256),
          Validators.minLength(0),  
          Validators.pattern(Utils.regex.address)      
        ]],
      email: ['',
        [
          Validators.required,
          Validators.maxLength(64),
          Validators.minLength(0),
          Validators.pattern(Utils.regex.email)
        ]],
      phone: ['',
        [
          Validators.required,
          Validators.maxLength(10),
          Validators.minLength(10),
          Validators.pattern(Utils.regex.phone)
        ]],
      birthdate: ['',
        [
          Validators.required,
        ]],
    })

    if (this.data != null) {
      this.title = 'EDITAR'
      this.form.get('id')?.setValue(this.data.id);
      this.form.get('document')?.setValue(this.data.document);
      this.form.get('gender')?.setValue(this.data.gender);
      this.form.get('name')?.setValue(this.data.name);
      this.form.get('address')?.setValue(this.data.address);
      this.form.get('email')?.setValue(this.data.email);
      this.form.get('phone')?.setValue(this.data.phone);
      this.form.get('birthdate')?.setValue(this.data.birthdate);
    }
  }
  
  onSave() {
    if (this.form.valid) {
      this.api.createOrUpdateCustomer(this.form.value).subscribe({
        next:(res) => {
          if (res.code === '001') {
            Swal.fire(res.message, '', 'warning')
          } else {
            this.form.reset();
            this._sharedService.showSnack({ title: 'Notificación', message: res.message });
            this.dialogRef.close({event: 'save', data: res});
          }
        },
        error:(err) => {
          this._sharedService.showSnack({ title: 'Notificación', message: err.message, type: 'danger' });
        }
      })
    }
  }

  numberOnly(event: any): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }
  
}
