import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ApiService } from 'src/app/services/api.service';
import { CreateCustomerComponent } from './create-customer.component';
import { CustomerI } from 'src/app/services/interfaces/customer.interface';
import { SharedService } from 'src/app/services/shared.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { FileI } from 'src/app/services/interfaces/file.interface';
import Swal from 'sweetalert2';
import * as XLSX from 'xlsx'

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css'],
})
export class CustomerComponent implements OnInit { 

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;  

  @ViewChild('fileInput') inputFile: ElementRef = {} as ElementRef;

  columns: string[] = [ 'name', 'document', 'gender', 'address', 'email', 'phone', 'birthdate', 'acciones'];
  dataSource: any;

  public name = '';

  constructor(
    private api: ApiService,
    private matDialog: MatDialog,
    private _sharedService: SharedService,
  ) {
  }

  ngOnInit(): void {    
    this.loadCustomers();
  }

  loadCustomers() {  
    this.api.getCustomers().subscribe({
      next:(res) => {
        this.loadTable(res)
      },
      error:(err) => {
        this._sharedService.showSnack({ title: 'Notificación', message: err.message, type: 'danger' });
      }
    })
  }

  loadTable(data: any) {
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.paginator = this.paginator;
    this.paginator._intl.itemsPerPageLabel = "Registros por página";
    this.paginator._intl.nextPageLabel = "Siguiente";
    this.paginator._intl.previousPageLabel = "Anterior";
    this.dataSource.sort = this.sort;
  }

  createCustomer() {
    const dialogRef = this.matDialog.open(CreateCustomerComponent, {
      width: '600px',   
      height: 'auto',
      data: null
    })
    dialogRef.afterClosed().subscribe((value) => {
      if (value && value.event === 'save') {
        this.loadCustomers()
      }
    });
  }

  editCustomer(customer: CustomerI) {
    const dialogRef = this.matDialog.open(CreateCustomerComponent, {
      width: '600px',   
      height: 'auto',
      data: customer
    })
    dialogRef.afterClosed().subscribe((value) => {
      if (value && value.event === 'save') {
        this.loadCustomers()
      }
    });
  }

  deleteCustomer(id: number) {
    Swal.fire({
      title: '¿Está seguro de eliminar?',
      text: "",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#2379cc',
      cancelButtonColor: '#e88e3a',
      confirmButtonText: 'Si',
      cancelButtonText: 'No'
    }).then((result) => {
      if (result.isConfirmed) {
        this.api.deleteCustomer(id).subscribe({
          next:(res) => {
            Swal.fire(res.message, '', res.code === '000' ? 'success' : 'warning')
            this.loadCustomers();
          },
          error:(err) => {
            Swal.fire(err.message, '', 'error')
          }
        })
      }
      else {
      }
    })
  }
  
  importFromExcel(event: any) {
    let customersFile: FileI[] = [];
    console.log('entrando a files')
    const file = event.target.files[0];
    if (file) {
      const reader = new FileReader();
      reader.readAsBinaryString(file);
      reader.onload = (event) => {
        let dataBinaria = event.target?.result;
        let workbook = XLSX.read(dataBinaria, { type: 'binary' });        
        workbook.SheetNames.forEach(sheet => {
          const data = XLSX.utils.sheet_to_json(workbook.Sheets[sheet], {                        
            raw: false,
            header: 0,
            dateNF: "yyyy-mm-dd"
          });
          const json = JSON.stringify(data);
          if (json.includes("Cedula") && json.includes("Nombre") && json.includes("Genero") && json.includes("Direccion") 
          && json.includes("Correo") && json.includes("Telefono") && json.includes("Fnacimiento")) {
            customersFile = JSON.parse(json);
            this.processImport(customersFile);
          } else {
            Swal.fire('Formato de archivo no valido!', '', 'warning')
          }
        })
      }
    } else {
      Swal.fire('No se ha seleccionado ningun archivo!', '', 'warning')
    }
    this.inputFile.nativeElement.value = "";
  }

  processImport(customersFile: FileI[]) {
    Swal.fire({
      title: '¿Se importaran ' + customersFile.length + ' cliente(s), ¿Está seguro de continuar?',
      text: "",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#2379cc',
      cancelButtonColor: '#e88e3a',
      confirmButtonText: 'Si',
      cancelButtonText: 'No'
    }).then((result) => {
      if (result.isConfirmed) {
        this.api.importCustomers(customersFile).subscribe({
          next:(res) => {
            console.log(res)
            Swal.fire(res.message, '', res.code === '000' ? 'success' : 'warning')
            this.loadCustomers();
          },
          error:(err) => {
            Swal.fire(err.message, '', 'error')
          }
        })
      }
    })
  }

}