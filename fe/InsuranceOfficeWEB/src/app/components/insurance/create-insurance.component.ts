import { Component, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ApiService } from 'src/app/services/api.service';
import { InsuranceI } from 'src/app/services/interfaces/insurance.interface';
import { SharedService } from 'src/app/services/shared.service';
import { Utils } from 'src/app/shared/utils';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-create-insurance',
  templateUrl: './create-insurance.component.html',
  styleUrls: ['./create-insurance.component.css']
})
export class CreateInsuranceComponent {

  defaultMessage: any = Utils.defaultMessages;
  form!: FormGroup;
  title = 'CREAR';

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: InsuranceI,
    public dialogRef: MatDialogRef<CreateInsuranceComponent>,
    private api: ApiService,
    private formBuilder: FormBuilder,
    private _sharedService: SharedService,
  ) {

  }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      id: [0],
      name: ['',
        [
          Validators.required,
          Validators.maxLength(32),
          Validators.minLength(4),
          Validators.pattern(Utils.regex.name)
        ]],
      code: ['',
        [
          Validators.required,
          Validators.maxLength(8),
          Validators.minLength(2),
          Validators.pattern(Utils.regex.code)
        ]],
      type: ['',
        [
          Validators.required,
        ]],
      insuredValue: ['',
        [
          Validators.required,
          Validators.maxLength(9),
          Validators.minLength(0),
          Validators.pattern(Utils.regex.number)
        ]],
      bonus: ['',
        [
          Validators.required,
          Validators.maxLength(9),
          Validators.minLength(0),
          Validators.pattern(Utils.regex.number)
        ]],
      observations: ['',
        [
          Validators.maxLength(256),
          Validators.pattern(Utils.regex.address)
        ]],
    })

    if (this.data != null) {
      this.title = 'EDITAR'
      this.form.get('id')?.setValue(this.data.id);
      this.form.get('name')?.setValue(this.data.name);
      this.form.get('code')?.setValue(this.data.code);
      this.form.get('type')?.setValue(this.data.type);
      this.form.get('insuredValue')?.setValue(this.data.insuredValue);
      this.form.get('bonus')?.setValue(this.data.bonus);
      this.form.get('observations')?.setValue(this.data.observations);
    }
  }
  
  onSave() {
    if (this.form.valid) {
      this.api.createOrUpdateInsurance(this.form.value).subscribe({
        next:(res) => {
          if (res.code === '001') {
            Swal.fire(res.message, '', 'warning')
          } else {
            this.form.reset();
            this._sharedService.showSnack({ title: 'Notificación', message: res.message });
            this.dialogRef.close({event: 'save', data: res});
          }
        },
        error:(err) => {
          Swal.fire(err.message, '', 'warning')
        }
      })
    }
  }

  numberOnly(event: any): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if ((charCode > 31 && (charCode < 48 || charCode > 57)) || charCode === 46) {
      return false;
    }
    return true;
  }  
  
}
