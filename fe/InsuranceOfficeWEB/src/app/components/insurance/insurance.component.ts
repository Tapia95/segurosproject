import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ApiService } from 'src/app/services/api.service';
import { CreateInsuranceComponent } from './create-insurance.component';
import { SharedService } from 'src/app/services/shared.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import Swal from 'sweetalert2';
import { InsuranceI } from 'src/app/services/interfaces/insurance.interface';

@Component({
  selector: 'app-insurance',
  templateUrl: './insurance.component.html',
  styleUrls: ['./insurance.component.css'],
})
export class InsuranceComponent implements OnInit { 

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;  

  columns: string[] = [ 'name', 'code', 'type', 'insuredValue', 'bonus', 'observations', 'acciones'];
  dataSource: any;

  insurances: InsuranceI[] = [];

  public name = '';

  constructor(
    private api: ApiService,
    private matDialog: MatDialog,
    private _sharedService: SharedService,
  ) {
  }

  ngOnInit(): void {    
    this.loadInsurances();
  }

  loadInsurances() {
    this.insurances = [];
    this.api.getInsurance().subscribe({
      next:(res) => {
        this.loadTable(res)
      },
      error:(err) => {
        this._sharedService.showSnack({ title: 'Notificación', message: err.message, type: 'danger' });
      }
    })
  }

  loadTable(data: any) {
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.paginator = this.paginator;
    this.paginator._intl.itemsPerPageLabel = "Registros por página";
    this.paginator._intl.nextPageLabel = "Siguiente";
    this.paginator._intl.previousPageLabel = "Anterior";
    this.dataSource.sort = this.sort;
  }
 
  createInsurance() {
    const dialogRef = this.matDialog.open(CreateInsuranceComponent, {
      width: '600px',   
      height: 'auto',
      data: null
    })
    dialogRef.afterClosed().subscribe((value) => {
      if (value && value.event === 'save') {
        this.loadInsurances()
      }
    });
  }

  editInsurance(insurance: InsuranceI) {
    const dialogRef = this.matDialog.open(CreateInsuranceComponent, {
      width: '600px',   
      height: 'auto',
      data: insurance
    })
    dialogRef.afterClosed().subscribe((value) => {
      if (value && value.event === 'save') {
        this.loadInsurances()
      }
    });
  }

  deleteInsurance(id: number) {
    Swal.fire({
      title: '¿Está seguro de eliminar?',
      text: "",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#2379cc',
      cancelButtonColor: '#e88e3a',
      confirmButtonText: 'Si',
      cancelButtonText: 'No'
    }).then((result) => {
      if (result.isConfirmed) {
        this.api.deleteInsurance(id).subscribe({
          next:(res) => {
            Swal.fire(res.message, '', res.code === '000' ? 'success' : 'warning' )
            this.loadInsurances();
          },
          error:(err) => {
            Swal.fire(err.message, '', 'error')
          }
        })
      }
      else {
      }
    })
  }

}