import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { UserStoreService } from 'src/app/services/user-store.service';

@Component({
  selector: 'app-nav-bar-custom',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {

  public name = '';

  constructor(
    private router: Router,  
    private auth: AuthService,
    private userStore: UserStoreService,
  ) {

  }

  ngOnInit(): void {
    // this.auth.getUsers().subscribe(res => {
    //   console.log(res)
    //   var users = res;
    // });

    // this.userStore.getNameFromStore().subscribe(val => {
    //   let fulName = this.auth.getNameFromToken();     
    //   this.name = val || fulName
    // })
  }

  onLogout() {
    localStorage.clear();
    this.router.navigate(['login'])
  }

}
