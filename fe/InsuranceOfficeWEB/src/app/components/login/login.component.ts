import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { UserStoreService } from '../../services/user-store.service';
import { SharedService } from '../../services/shared.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {

  loginForm!: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private auth: AuthService,
    private router: Router,
    private userStore: UserStoreService,
    private _sharedService: SharedService,
  ) {
  }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    })
  }

  onSubmit(): void {
    if (this.loginForm.valid) {
      this.auth.login(this.loginForm.value).subscribe({
        next:(res) => {
          this.loginForm.reset();
          this.auth.storeToken(res.token);
          const payload = this.auth.decodeToken();
          this.userStore.setNameFromStore(payload.unique_name);
          this._sharedService.showSnack({ title: 'Notificación', message: res.message });
          this.router.navigate(['policies']);
        },
        error:() => {
          this._sharedService.showSnack({ title: 'Atención', message: 'Usuario o contraseña no validos', type: 'danger' });
        }
      })
    } else {      
      this._sharedService.showSnack({ title: 'Notificación', message: 'Por favor completar los campos', type: 'warning' });
    }
  }

}