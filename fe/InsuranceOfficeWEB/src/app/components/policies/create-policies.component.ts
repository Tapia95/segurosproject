import { SelectionModel } from '@angular/cdk/collections';
import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { ApiService } from 'src/app/services/api.service';
import { CustomerI } from 'src/app/services/interfaces/customer.interface';
import { InsuranceI } from 'src/app/services/interfaces/insurance.interface';
import { PolicyI } from 'src/app/services/interfaces/policy.interface';
import { SharedService } from 'src/app/services/shared.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-create-policies',
  templateUrl: './create-policies.component.html',
  styleUrls: ['./create-policies.component.css']
})
export class CreatePoliciesComponent {

  columns: string[] = [ 'select', 'name', 'code', 'type', 'insuredValue', 'bonus' ];
  dataSource: any;

  columnsCustomer: string[] = [ 'select', 'name', 'document', 'gender', 'email', 'phone' ];
  dataSourceCustomer: any;

  customerSelected: any = null;
    
  customerList: CustomerI[] = [];
  insuranceList: InsuranceI[] = [];
  selection: SelectionModel<any>;

  completeStep1: boolean = false;

  constructor(
    public dialogRef: MatDialogRef<CreatePoliciesComponent>,
    private api: ApiService,
    private formBuilder: FormBuilder,
    private _sharedService: SharedService,
  ) {
    this.selection = new SelectionModel<any>(true, []);
    this.getCustomers();
    this.getInsurances();
  } 

  ngOnInit(): void {  
  }

  getCustomers() {
    this.api.getCustomers().subscribe({
      next:(res) => {
        this.customerList = res;
        this.loadTableCustomer(res)
      },
      error:(err) => {
        this._sharedService.showSnack({ title: 'Notificación', message: err.message, type: 'danger' });
      }
    })
  }

  getInsurances() {
    this.api.getInsurance().subscribe({
      next:(res) => {
        this.insuranceList = res;
        this.loadTable(res)
      },
      error:(err) => {
        this._sharedService.showSnack({ title: 'Notificación', message: err.message, type: 'danger' });
      }
    })
  }

  loadTable(data: any) {
    this.dataSource = new MatTableDataSource(data);
  }

  loadTableCustomer(data: any) {
    this.dataSourceCustomer = new MatTableDataSource(data);
  }
  
  onSave() {    
    if (!this.customerSelected) {
      Swal.fire('Atención', 'Seleccione un cliente por favor', 'warning')
      return;
    }
    if (this.selection.isEmpty()) {
      Swal.fire('Atención', '¡No se ha seleccionado ningun seguro!', 'warning')
      return;
    }

    let policyLst: PolicyI[] = [];
    this.selection.selected.forEach(x => {
      let policy: PolicyI = {id: 0, policyNumber: '', customerId: this.customerSelected.id, InsuranceId: x.id }
      policyLst.push(policy);
    })

    this.api.createOrUpdatePolicies(policyLst).subscribe({
      next:(res) => {
        this._sharedService.showSnack({ title: 'Notificación', message: res.message });
        this.dialogRef.close({event: 'save', data: res});
      },
      error:(err) => {
        this._sharedService.showSnack({ title: 'Notificación', message: err.message, type: 'danger' });
      }
    })
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach((row: any) => this.selection.select(row));
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }
  
  changeCustomer(data: any) {
    this.customerSelected = data;
    if (this.customerSelected) {
      this.completeStep1 = true;
    } else {
      this.completeStep1 = false;
    }
  }

  filterCustomer(event: any) {
    const filterValue = event.target.value;
    this.dataSourceCustomer.filter = filterValue.trim().toLowerCase();
  }

  filterInsurance(event: any) {
    const filterValue = event.target.value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
