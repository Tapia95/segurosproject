import { Component, Inject, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { PolicyViewI } from 'src/app/services/interfaces/policy-view.interface';

@Component({
  selector: 'app-policies-view',
  templateUrl: './policies-view.component.html',
  styleUrls: ['./policies-view.component.css']
})
export class PoliciesViewComponent {

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  form!: FormGroup;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: PolicyViewI,
    public dialogRef: MatDialogRef<PoliciesViewComponent>, 
    private formBuilder: FormBuilder,
  ) {
  }

  ngOnInit(): void {
    this.form = this.formBuilder.group({      
      documentCustomer: [this.data.documentCustomer, [ ]],
      insuranceName: [this.data.insuranceName, [ ]],
      insuranceType: [this.data.insuranceType, [ ]],
      insuredValue: [this.data.insuredValue, [ ]],
      nameCustomer: [this.data.nameCustomer, [ ]],  
      observations: [this.data.observations, [ ]],  
      policyNumber: [this.data.policyNumber, [ ]],
    })
  }
  
}
