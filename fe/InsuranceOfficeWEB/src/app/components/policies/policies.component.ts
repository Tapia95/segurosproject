import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ApiService } from 'src/app/services/api.service';
import { SharedService } from 'src/app/services/shared.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import Swal from 'sweetalert2';
import { CreatePoliciesComponent } from './create-policies.component';
import { PoliciesViewComponent } from './policies-view.component';

@Component({
  selector: 'app-policies',
  templateUrl: './policies.component.html',
  styleUrls: ['./policies.component.css'],
})
export class PoliciesComponent implements OnInit { 

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;  

  columns: string[] = [ 'policyNumber', 'documentCustomer', 'nameCustomer', 'insuranceCode', 'insuranceName', 'insuranceType', 'insuredValue', 'observations', 'acciones'];
  dataSource: any;

  selectedFilter: string = 'document'; //default
  filterText: string = ''; 
  public name = '';

  constructor(
    private api: ApiService,
    private matDialog: MatDialog,
    private _sharedService: SharedService,
  ) {
  }

  ngOnInit(): void {    
    this.loadPolicies();
  }

  loadPolicies() {
    this.filterText = '';
    this.api.getPolicies().subscribe({
      next:(res) => {
        this.loadTable(res)
      },
      error:(err) => {
        this._sharedService.showSnack({ title: 'Notificación', message: err.message, type: 'danger' });
      }
    })
  }

  filterPolicy() {
    let objFilter;

    // si esta vacio carga todo
    if (!this.filterText) {
      this.loadPolicies();
      return;
    }
    
    if (this.selectedFilter === 'document') {
      objFilter = { 'document' : this.filterText }
      this.api.getPoliciesByCi(objFilter).subscribe({
        next:(res) => {
          if (res.code === '001') {
            Swal.fire(res.message, '', 'warning' )
          } else {
            this.loadTable(res.data)
          }
        },
        error:(err) => {
          Swal.fire(err.message, '', 'warning' )
        }
      })
    } else {
      objFilter = { 'code' : this.filterText }
      this.api.getPoliciesByCode(objFilter).subscribe({
        next:(res) => {
          if (res.code === '001') {
            Swal.fire(res.message, '', 'warning' )
          } else {
            this.loadTable(res.data)
          }
        },
        error:(err) => {
          Swal.fire(err.message, '', 'warning' )
        }
      })
    }
  }

  loadTable(data: any) {
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.paginator = this.paginator;
    this.paginator._intl.itemsPerPageLabel = "Registros por página";
    this.paginator._intl.nextPageLabel = "Siguiente";
    this.paginator._intl.previousPageLabel = "Anterior";
    this.dataSource.sort = this.sort;
  }
 
  createPolicy() {
    const dialogRef = this.matDialog.open(CreatePoliciesComponent, {
      width: '820px',
      height: 'auto',
      maxHeight: '720px',
      data: null
    })
    dialogRef.afterClosed().subscribe((value) => {
      if (value && value.event === 'save') {
        this.loadPolicies()
      }
    });
  }

  viewPolicy(data: any) {
    this.matDialog.open(PoliciesViewComponent, {
      width: '600px',
      height: 'auto',
      data: data
    })
  }

  deletePolicy(id: number) {
    Swal.fire({
      title: '¿Está seguro de eliminar?',
      text: "",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#2379cc',
      cancelButtonColor: '#e88e3a',
      confirmButtonText: 'Si',
      cancelButtonText: 'No'
    }).then((result) => {
      if (result.isConfirmed) {
        this.api.deletePolicy(id).subscribe({
          next:(res) => {
            Swal.fire(res.message, '', 'success')
            this.loadPolicies();
          },
          error:(err) => {
            Swal.fire(err.message, '', 'error')
          }
        })
      }
      else {
      }
    })
  }

}