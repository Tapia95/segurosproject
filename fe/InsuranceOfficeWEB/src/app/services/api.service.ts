import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CustomerI } from './interfaces/customer.interface';
import { environment } from 'src/environment';
import { PolicyI } from './interfaces/policy.interface';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private baseCustomerUrl: string = environment.api.url + 'customer';
  private baseInsuranceUrl: string = environment.api.url + 'insurance';
  private basePolicyUrl: string = environment.api.url + 'policy';
  
  constructor(private _http: HttpClient) { }

  createOrUpdateCustomer(customer: CustomerI) {
    return this._http.post<any>(`${this.baseCustomerUrl}/createOrUpdate`, customer);
  }

  deleteCustomer(id: number) {
    let params = new HttpParams() .append("id", id.toString())
    return this._http.delete<any>(`${this.baseCustomerUrl}/delete?${id}`, {params});
  }

  getCustomers() {
    return this._http.get<any>(this.baseCustomerUrl);
  }

  importCustomers(customers: any) {
    return this._http.post<any>(`${this.baseCustomerUrl}/importCustomers`, customers);
  }

  createOrUpdateInsurance(customer: CustomerI) {
    return this._http.post<any>(`${this.baseInsuranceUrl}/createOrUpdate`, customer);
  }

  deleteInsurance(id: number) {
    let params = new HttpParams() .append("id", id.toString())
    return this._http.delete<any>(`${this.baseInsuranceUrl}/delete?${id}`, {params} );
  }

  getInsurance() {
    return this._http.get<any>(this.baseInsuranceUrl);
  }

  createOrUpdatePolicies(policies: PolicyI[]) {
    return this._http.post<any>(`${this.basePolicyUrl}/createOrUpdate`, policies);
  }

  getPolicies() {
    return this._http.post<any>(`${this.basePolicyUrl}/findAll`, null);
  }

  getPoliciesByCi(filter: any) {
    return this._http.post<any>(`${this.basePolicyUrl}/findByDocument`, filter);
  }

  getPoliciesByCode(filter: any) {
    return this._http.post<any>(`${this.basePolicyUrl}/findByCodeInsurance`, filter);
  }

  deletePolicy(id: number) {
    let params = new HttpParams() .append("id", id.toString())
    return this._http.delete<any>(`${this.basePolicyUrl}/delete`, {params} );
  }
  
}
