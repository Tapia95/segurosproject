import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { environment } from 'src/environment';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private baseUrl: string = environment.api.url + 'user';
  private userPayload: any;
  constructor(private _http: HttpClient) {
    this.userPayload = this.decodeToken();
  }

  login(userObj: any) {
    return this._http.post<any>(`${this.baseUrl}/authenticate`, userObj);
  }

  register(userObj: any) {
    return this._http.post<any>(`${this.baseUrl}/register`, userObj);
  }

  getUsers() {
    return this._http.get<any>(this.baseUrl);
  }

  storeToken(tokenValue: string) {
    localStorage.setItem('token', tokenValue);
  }

  getToken() {
    return localStorage.getItem('token');
  }

  isLoggedIn(): boolean {
    return !!localStorage.getItem('token');
  }

  decodeToken() {
    const jwtHelper = new JwtHelperService();
    const token = this.getToken()!;
    return jwtHelper.decodeToken(token);
  }

  getNameFromToken() {
    if (this.userPayload) {
      return this.userPayload.name;
    }
  }

  getRoleFromToken() {
    if (this.userPayload) {
      return this.userPayload.role;
    }
  }
}
