export interface AlertI {
    title: string,
    message: string,
    type?: 'success' | 'warning' | 'danger'
}