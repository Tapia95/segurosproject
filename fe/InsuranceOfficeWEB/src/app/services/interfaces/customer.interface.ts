export interface CustomerI {
  id: number;
  name: string;
  document: string;
  address: string;
  email: string;
  gender: string;
  phone: string;
  birthdate: Date;
}
