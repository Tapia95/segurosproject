export interface InsuranceI {
  id: number;
  name: string;
  code: string;
  type: string;
  insuredValue: number;
  bonus: number;
  observations: string;
}
