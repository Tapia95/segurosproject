export interface PolicyViewI {
  id: number;
  policyNumber: string;
  documentCustomer: string;
  nameCustomer: string;
  insuranceName: string;
  insuranceCode: string;
  insuranceType: string;
  insuredValue: number;
  observations: string;
}
