import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { AlertI } from './interfaces/alert.interface';
import { SnackBarComponent } from '../components/layout/snack-bar/snack-bar.component';
import { AlertComponent } from '../components/layout/alert/alert.component';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  constructor(
    private _matDialog: MatDialog,
    private _snackBar: MatSnackBar
  ) { } 

  showAlert(alertContent: AlertI) {
    this._matDialog.open(AlertComponent, {
      disableClose: true,
      hasBackdrop: true,
      width: '420px',
      height: '400px',
      data: { ...alertContent },
      panelClass: ['alertModal']
    })
  }

  showSnack(
    snackContent: AlertI,
    snackType: 'success' | 'warning' | 'danger' = 'success',
    horizontalPosition: MatSnackBarHorizontalPosition = 'end',
    verticalPosition: MatSnackBarVerticalPosition = 'bottom',
    seconds: number = 1800) {
    if (!snackContent.type) snackContent.type = snackType;
    this._snackBar.openFromComponent(SnackBarComponent, {
      data: {
        ...snackContent
      },
      duration: seconds,
      horizontalPosition,
      verticalPosition,
      panelClass: [`alert-${snackContent.type}`]
    })
  }

}
