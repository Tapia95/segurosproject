
export abstract class Utils {
    public static regex = {
        document: /^\d{10}$/,  
        name: /^[a-zA-ZÀ-ÿ\s]{4,64}$/,
        address: /^[a-zA-Z0-9À-ÿ\s]{0,256}$/,
        email: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
        phone: /^\d{10}$/,
        code: /^[a-zA-Z0-9À-ÿ\s]{2,8}$/,
        number: /^\d*\.?\d*$/,
      }

    public static defaultMessages = {
        requiredField: 'campo obligatorio',
        invalidField: 'campo no valido',
        maxLength: 'número de caracteres excedidos',
        minLength: 'no cumple con el minimo de caracteres',
        emailInvaild: 'correo no valido',
        minMaxNumber: 'valores permitidos entre 0 y 10000',
    }
}


