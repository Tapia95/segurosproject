USE [master]
GO
/****** Object:  Database [InsuranceOfficeDB]    Script Date: 3/7/2023 18:14:34 ******/
CREATE DATABASE [InsuranceOfficeDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'InsuranceOfficeBD', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL16.SQLEXPRESS\MSSQL\DATA\InsuranceOfficeBD.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'InsuranceOfficeBD_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL16.SQLEXPRESS\MSSQL\DATA\InsuranceOfficeBD_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT, LEDGER = OFF
GO
ALTER DATABASE [InsuranceOfficeDB] SET COMPATIBILITY_LEVEL = 160
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [InsuranceOfficeDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [InsuranceOfficeDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [InsuranceOfficeDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [InsuranceOfficeDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [InsuranceOfficeDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [InsuranceOfficeDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [InsuranceOfficeDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [InsuranceOfficeDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [InsuranceOfficeDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [InsuranceOfficeDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [InsuranceOfficeDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [InsuranceOfficeDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [InsuranceOfficeDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [InsuranceOfficeDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [InsuranceOfficeDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [InsuranceOfficeDB] SET  DISABLE_BROKER 
GO
ALTER DATABASE [InsuranceOfficeDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [InsuranceOfficeDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [InsuranceOfficeDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [InsuranceOfficeDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [InsuranceOfficeDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [InsuranceOfficeDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [InsuranceOfficeDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [InsuranceOfficeDB] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [InsuranceOfficeDB] SET  MULTI_USER 
GO
ALTER DATABASE [InsuranceOfficeDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [InsuranceOfficeDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [InsuranceOfficeDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [InsuranceOfficeDB] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [InsuranceOfficeDB] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [InsuranceOfficeDB] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [InsuranceOfficeDB] SET QUERY_STORE = ON
GO
ALTER DATABASE [InsuranceOfficeDB] SET QUERY_STORE (OPERATION_MODE = READ_WRITE, CLEANUP_POLICY = (STALE_QUERY_THRESHOLD_DAYS = 30), DATA_FLUSH_INTERVAL_SECONDS = 900, INTERVAL_LENGTH_MINUTES = 60, MAX_STORAGE_SIZE_MB = 1000, QUERY_CAPTURE_MODE = AUTO, SIZE_BASED_CLEANUP_MODE = AUTO, MAX_PLANS_PER_QUERY = 200, WAIT_STATS_CAPTURE_MODE = ON)
GO
USE [InsuranceOfficeDB]
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 3/7/2023 18:14:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[customers]    Script Date: 3/7/2023 18:14:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[customers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Document] [nvarchar](max) NULL,
	[Gender] [nvarchar](max) NULL,
	[Address] [nvarchar](max) NULL,
	[Email] [nvarchar](max) NULL,
	[Phone] [nvarchar](max) NULL,
	[Birthdate] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_customers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[insurances]    Script Date: 3/7/2023 18:14:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[insurances](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Code] [nvarchar](max) NULL,
	[Type] [nvarchar](max) NULL,
	[InsuredValue] [float] NOT NULL,
	[Bonus] [float] NOT NULL,
	[Observations] [nvarchar](max) NULL,
 CONSTRAINT [PK_insurances] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[policies]    Script Date: 3/7/2023 18:14:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[policies](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[InsuranceId] [int] NOT NULL,
	[PolicyNumber] [nvarchar](max) NULL,
 CONSTRAINT [PK_policies] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[users]    Script Date: 3/7/2023 18:14:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[users](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Username] [nvarchar](max) NULL,
	[Password] [nvarchar](max) NULL,
	[Token] [nvarchar](max) NULL,
	[Role] [nvarchar](max) NULL,
	[Email] [nvarchar](max) NULL,
	[Phone] [nvarchar](max) NULL,
	[Name] [nvarchar](max) NULL,
 CONSTRAINT [PK_users] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20230701210317_CreateTableUser', N'7.0.8')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20230702024327_AddFieldNames', N'7.0.8')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20230702062106_CreateTableCustomer', N'7.0.8')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20230703041251_AddTableInsurancesFix', N'7.0.8')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20230703143418_AddTablePolices', N'7.0.8')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20230703143820_AddFieldPolicyNumber', N'7.0.8')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20230703144557_FixNameTablePolicies', N'7.0.8')
GO
SET IDENTITY_INSERT [dbo].[customers] ON 

INSERT [dbo].[customers] ([Id], [Name], [Document], [Gender], [Address], [Email], [Phone], [Birthdate]) VALUES (2, N'Andres Carlos Hurtado Gomez', N'0951963272', N'M', N'Coop nueva esperanza mz1995', N'correouser@email.com', N'0960457896', CAST(N'2000-01-01T05:00:00.0000000' AS DateTime2))
SET IDENTITY_INSERT [dbo].[customers] OFF
GO
SET IDENTITY_INSERT [dbo].[insurances] ON 

INSERT [dbo].[insurances] ([Id], [Name], [Code], [Type], [InsuredValue], [Bonus], [Observations]) VALUES (7, N'Seguro Uno', N'00000A', N'life', 152.2, 500.12, N'Todo OK')
SET IDENTITY_INSERT [dbo].[insurances] OFF
GO
SET IDENTITY_INSERT [dbo].[policies] ON 

INSERT [dbo].[policies] ([Id], [CustomerId], [InsuranceId], [PolicyNumber]) VALUES (8, 2, 7, N'00000000')
SET IDENTITY_INSERT [dbo].[policies] OFF
GO
SET IDENTITY_INSERT [dbo].[users] ON 

INSERT [dbo].[users] ([Id], [Username], [Password], [Token], [Role], [Email], [Phone], [Name]) VALUES (1, N'sa', N'123qwe', N'xxxxxxxxxxxxxx', N'ADMIN', N'test@email.com', N'0960597318', N'ADMIN ADMIN')
SET IDENTITY_INSERT [dbo].[users] OFF
GO
/****** Object:  Index [IX_policies_CustomerId]    Script Date: 3/7/2023 18:14:34 ******/
CREATE NONCLUSTERED INDEX [IX_policies_CustomerId] ON [dbo].[policies]
(
	[CustomerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_policies_InsuranceId]    Script Date: 3/7/2023 18:14:34 ******/
CREATE NONCLUSTERED INDEX [IX_policies_InsuranceId] ON [dbo].[policies]
(
	[InsuranceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
ALTER TABLE [dbo].[policies]  WITH CHECK ADD  CONSTRAINT [FK_policies_customers_CustomerId] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[customers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[policies] CHECK CONSTRAINT [FK_policies_customers_CustomerId]
GO
ALTER TABLE [dbo].[policies]  WITH CHECK ADD  CONSTRAINT [FK_policies_insurances_InsuranceId] FOREIGN KEY([InsuranceId])
REFERENCES [dbo].[insurances] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[policies] CHECK CONSTRAINT [FK_policies_insurances_InsuranceId]
GO
/****** Object:  StoredProcedure [dbo].[GET_SEG_USER]    Script Date: 3/7/2023 18:14:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GET_SEG_USER]

	@user VARCHAR (50),
	@password VARCHAR (50)
AS
BEGIN

	SELECT usr.Username, usr.Password FROM SegUser as usr 
	WHERE usr.Username = @user AND  usr.Password = @password

END
GO
USE [master]
GO
ALTER DATABASE [InsuranceOfficeDB] SET  READ_WRITE 
GO
